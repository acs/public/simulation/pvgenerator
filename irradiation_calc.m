%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to calculate total irradiation
%
% @author Marco Pau <mpau@eonerc.rwth-aachen.de>
% @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% PVgenerator
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [GHI, DNI, DHI, Extraterr_radiation] = irradiation_calc(num_day, theta_zenith, cloudiness)

% Air mass calculation: reference of formula -> formula of Young (1994) on: https://en.wikipedia.org/wiki/Air_mass_%28astronomy%29#CITEREFYoung1994
% air_mass = (1.002432.*(cos(theta_zenith).^2) + 0.148386.*cos(theta_zenith) + 0.0096467) ./ ((cos(theta_zenith).^3) + 0.149864.*(cos(theta_zenith).^2) + 0.0102963.*cos(theta_zenith) + 0.000303978);
% air_mass(theta_zenith>pi/2) = -1;

Esolar_constant = 1367;             % Energy solar constant in Watt per squared meter
b = 2*pi*num_day/365;
R = 1.00011 + 0.034221*cos(b) + 0.00128*sin(b) + 0.000719*cos(2*b) + 0.000077*sin(2*b);     % Multiplicative factor taken from: https://pvpmc.sandia.gov/modeling-steps/1-weather-design-inputs/irradiance-and-insolation-2/extraterrestrial-radiation/

Extraterr_radiation = Esolar_constant*R;    % in Watts per squared meters, taken from: https://pvpmc.sandia.gov/modeling-steps/1-weather-design-inputs/irradiance-and-insolation-2/extraterrestrial-radiation/

[DNI] = DNI_calculation(Extraterr_radiation, theta_zenith);     % Direct Normal Irradiance (normal to the sun beam)

[DHI] = DHI_calculation(Extraterr_radiation, theta_zenith);     % Diffuse Horizontal Irradiance (on a horizontal surface)

DNI(DNI<0.01) = 0;
idx = find(DNI(1:43200,1)==0);
len = length(idx);
idx_start = idx(len);
DNI(1:idx_start,1) = zeros(idx_start,1);
DHI(1:idx_start,1) = zeros(idx_start,1);
idx = find(DNI(43201:end,1)==0);
idx_end = idx(1)+43200;
len = 86400-idx_end;
DNI(idx_end+1:end,1) = zeros(len,1);
DHI(idx_end+1:end,1) = zeros(len,1);

[DNI, DHI] = cloud_generator(DNI, DHI, cloudiness);

GHI = DHI + DNI.*cos(theta_zenith);

% DNIh = DNI.*cos(theta_zenith);



