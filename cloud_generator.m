%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to calculate impact of clouds
%
% @author Marco Pau <mpau@eonerc.rwth-aachen.de>
% @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% PVgenerator
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [DNI, DHI] = cloud_generator(DNI, DHI, cloudiness)
DNI_start = DNI;
DHI_start = DHI;
load('Mean_diffuse.mat');
load('Mean_direct.mat');
mean_dir_tot(1,1) = 0;
mean_dif_tot(1,1) = 0;

for i = 1:size(cloudiness,1)
    start_time = cloudiness(i,1)*3600;
    end_time = cloudiness(i,2)*3600;
    [d_dir, d_dif] = reduction_factor_creation_new(start_time, end_time, cloudiness(i,3));
    DNI(start_time+1:end_time,1) = (d_dir' + mean_dir_tot(cloudiness(i,3),1)).*DNI_start(start_time+1:end_time,1) + DNI_start(start_time+1:end_time,1);
    DHI(start_time+1:end_time,1) = (d_dif' + mean_dif_tot(cloudiness(i,3),1)).*DNI_start(start_time+1:end_time,1) + DHI_start(start_time+1:end_time,1);
    
    for x = start_time+1 : end_time
        if DNI(x) > DNI_start(x)
           delta_dir = DNI(x) - DNI_start(x);
            DNI(x) =  DNI_start(x) - delta_dir;
        end
        if DNI(x) < 0
            DNI(x) = 0;
        end
       if DHI(x) < 0
            DHI(x) =  - DHI(x);
       end
    end
    
    var =300;
    if start_time>var
        for j = 1:var
            DNI(start_time+j,1) = ((var-j)/var)*DNI(start_time,1) + (j/var)*DNI(start_time+j,1);
            DHI(start_time+j,1) = ((var-j)/var)*DHI(start_time,1) + (j/var)*DHI(start_time+j,1);
        end
    end
end


% DHI_start(DHI_start<0) = 0;
% DHI(DHI<0) = 0;
% DNI_start(DNI_start<0) = 0;
% DNI(DNI<0) = 0;
% figure(1)
% hold on
% plot(DNI,'b')
% plot(DHI,'g')
% axis([20000 70000 0 900])
% a = DHI;
