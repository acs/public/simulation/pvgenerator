%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to calculate PV output
%
% @author Marco Pau <mpau@eonerc.rwth-aachen.de>
% @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% PVgenerator
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [PV_output] = POA_irradiance(GHI, DNI, DHI, theta_zenith, theta_azimuth, extraterr_radiation, albedo, tilt_pv, azimuth_pv)

tilt_pv = tilt_pv*pi/180;
azimuth_pv = azimuth_pv*pi/180;
AOI = acos(cos(theta_zenith).*cos(tilt_pv) + sin(theta_zenith)*sin(tilt_pv).*cos(theta_azimuth-azimuth_pv));
Ai = DNI/extraterr_radiation;
Eb = DNI.*cos(AOI);
Ed = DHI.*(Ai.*cos(AOI) + (1-Ai).*(1+cos(tilt_pv))/2);
% Eg = 0;
Eg = GHI*albedo*(1-cos(tilt_pv))/2;
POA = Eb + Eg + Ed;
POA(POA<0) = 0;

if mod(length(POA),2) == 1
    POA1 = POA(1:length(POA)/2-0.5);
    POA2 = POA(length(POA)/2+0.5:end);
elseif mod(length(POA),2) == 0
    POA1 = POA(1:length(POA)/2);
    POA2 = POA(length(POA)/2+1:end);
end

idx1 = find(POA1==0);
len_idx1 = length(idx1);
idx2 = idx1(len_idx1,1);
POA1(1:idx2,1) = 0;

idx1 = find(POA2==0);
idx2 = idx1(1,1);
POA2(idx2:end,1) = 0;

PV_output = [POA1; POA2]/1000;
% PV_output = [POA1; POA2];

