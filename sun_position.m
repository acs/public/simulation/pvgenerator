%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to calculate sun position
%
% @author Marco Pau <mpau@eonerc.rwth-aachen.de>
% @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% PVgenerator
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [theta_zenith, theta_azimuth] = sun_position(num_day, lat, lon, GMT_diff)

% Inputs:   num_day = number of the day of the year
%           lat = latitude of the considered location in decimal degrees (northern emisphere has positive values)
%           lon = longitude of the considered location in decimal degrees (east with repect to Greenwich has positive values)
%           GMT_diff = Greenwich Mean Time, time offset with respect to UTC
% Outputs:  theta_zenith = zenith angle of the sun (in degrees)
%           theta_azimuth = azimuth angle of the sun (in degrees)
%           air mass = air mass value

% Tlocal=[0:0.01:24]';                                % time of the day with resolution of 1 second
Tlocal=[24/86400:24/86400:24]';

theta_d= (23.45*pi/180)*sin(2*pi*(284+num_day)/365);    % approximated calculation of the declination angle of the sun (in radians) according to the number of the day (PV Sandia model at: https://pvpmc.sandia.gov/modeling-steps/1-weather-design-inputs/sun-position/simple-models/)

if(num_day >= 1 && num_day <= 106)                                      % approximated calculation of the equation of time according to the number of the day (PV Sandia model at: https://pvpmc.sandia.gov/modeling-steps/1-weather-design-inputs/sun-position/simple-models/)
    eq_time = -14.2*sin(pi*(num_day+7)/111);

elseif(num_day > 106 && num_day <= 166)
    eq_time = 4.0*sin(pi*(num_day-106)/59);

elseif(num_day > 166 && num_day <= 246)
    eq_time = -6.5*sin(pi*(num_day-166)/80);

elseif(num_day > 246 && num_day <= 366) %andrea --> modified from 365 to 366
    eq_time = 16.4*sin(pi*(num_day-247)/113);
end

lon_sm = 15*(GMT_diff);                                  % longitude of the standard meridian of the observerís time zone (degrees)
Tsolar = Tlocal+eq_time/60+(lon-lon_sm)/15;              % Calculation of the solar time based on the position of the sun and not on the local time (wrong sign in Sandia website)

theta_hr = pi*(Tsolar-12)/12;                            % Calculation of the hour angle (in radians) (wrong sign in Sandia website)

theta_zenith = acos(sin(lat*pi/180)*sin(theta_d)+cos(lat*pi/180)*cos(theta_d).*cos(theta_hr));    % Solar zenith angle (in radians) accordin to PV Sandia model (https://pvpmc.sandia.gov/modeling-steps/1-weather-design-inputs/sun-position/simple-models/)
% Possible validation of zenith angles at: https://midcdmz.nrel.gov/solpos/spa.html

theta_azimuth1 = acos((sin(theta_d)*cos(lat*pi/180)-cos(theta_d)*sin(lat*pi/180).*cos(theta_hr))./sin(theta_zenith)); % Calculation of the solar azimuth angle (Wikipedia)
theta_azimuth2 = asin(-sin(theta_hr).*cos(theta_d)./sin(theta_zenith));
theta_azimuth = theta_azimuth1; % just for initialization purposes
% If procedure to put the azimuth between 0 and pi when the hour angle is negative and between -pi and 0 when the hour angle is positive
for i = 1:length(theta_azimuth)
    if theta_azimuth2(i,1) < 0 && theta_azimuth1(i,1) < pi/2
        theta_azimuth(i,1) = 2*pi + theta_azimuth2(i,1);
    elseif theta_azimuth2(i,1) < 0 && theta_azimuth1(i,1) > pi/2
        theta_azimuth(i,1) = 2*pi - theta_azimuth1(i,1);
    end
end

% theta_zenith=theta_zenith *180/pi;
% theta_azimuth=theta_azimuth *180/pi;

