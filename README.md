# Software tool to generate PV power profiles

This repository contains the following material:

- Matlab code to generate realistic PV power injection profiles for 1 day. 

## Instructions 
1) Run in the command window "PVgenerator.m"
2) Define, in the GUI, the following parameters:
	- Location inputs. Latitude, longitude
	- Time inputs Time of the year
	- Cloudiness settings: Cloudiness level in different parts of the day
        -  1 = clear sky conditions
        -  2 = slightly cloudy conditions
        -  3 = cloudy conditions
        -  4 = overcast conditions
	- PV settings: Geometrical placement and power settings of PV panels
3) Press "Generate profiles".
4) This will generate a figure with the profile in the GUi and create a file "PV_Power_profiles.mat" with all the power profiles, with time resolution of 1 second, in the current directory.

## Copyright

2019, Institute for Automation of Complex Power Systems, EONERC  

## License

This project is released under the terms of the [GPL version 3](LICENSE).

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

For other licensing options please consult [Prof. Antonello Monti](mailto:amonti@eonerc.rwth-aachen.de).

## Contact

[![EONERC ACS Logo](https://git.rwth-aachen.de/acs/public/villas/VILLASnode/raw/develop/doc/pictures/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- Marco Pau <mpau@eonerc.rwth-aachen.de>
- Andrea Angioni <aangioni@eonerc.rwth-aachen.de>

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[EON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de) 




