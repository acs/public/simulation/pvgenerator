%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to generate base power profile
%
% @author Marco Pau <mpau@eonerc.rwth-aachen.de>
% @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% PVgenerator
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [DNI,DHI] = Gen_Base_Profile(num_day)
% GHI_matrix = zeros(2401,365);
% POA_matrix = zeros(2401,365);

% lat = 50.77;
% lon = 6.09;
% lat = 39.2167;
% lon = 9.1167;
lat = 36.06;
lon = -115.08;
GMT_diff = -8;
albedo = 0.2;
tilt_pv = 30;
azimuth_pv = 180;

%1 sunny
%2 slightly cloudy
%3 cloudy
%4 total overcast
% cloudiness = [0   10    4;
%              10  12.75, 3;
%              12.75 13.2 2;
%              13.2 24 1];

% cloudiness = [0   8.5    2;
%              8.5  12.0, 2;
%              12.0 13.0 2;
%              13.0 15.5  3;
%              15.5 24    3];

cloudiness = [ 0 24 1];

% for i =1:365
    %num_day = 33;     %20  79  171
    [theta_zenith, ~] = sun_position(num_day, lat, lon, GMT_diff);
    [~, DNI, DHI, ~] = irradiation_calc_test(num_day, theta_zenith, cloudiness);
%    [PV_output] = POA_irradiance(GHI, DNI, DHI, theta_zenith, theta_azimuth, extraterr_radiation, albedo, tilt_pv, azimuth_pv);
%     GHI_matrix(:,i) = GHI;
%     POA_matrix(:,i) = POA;
% end



end
