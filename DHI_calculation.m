%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to calculate direct sun irradiation
%
% @author Marco Pau <mpau@eonerc.rwth-aachen.de>
% @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% PVgenerator
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [DHI] = DHI_calculation(Extraterr_radiation, theta_zenith)

% Tlk = 4.5;    % value of Linke's turbidity factor when the sky is fully clean and dry (cit. Comparison Between Atmospheric Turbidity Coefficients of Desert and Temperate Climates)
Tlk = 3;    
h0 = (pi/2)-theta_zenith;
Tn = -0.015843 + 0.030543*Tlk + 0.0003797*(Tlk^2);  % this and folllowing values are collected from: http://re.jrc.ec.europa.eu/pvgis/solres/solmod3.htm
A1 = 0.26463 - 0.061581*Tlk + 0.0031408*(Tlk^2);
A2 = 2.04020 + 0.018945*Tlk - 0.011161*(Tlk^2);
A3 = -1.3025 + 0.039231*Tlk + 0.0085079*(Tlk^2);
Fd = A1 + A2.*sin(h0) + A3.*(sin(h0).^2);

DHI = Extraterr_radiation*Tn*Fd; 