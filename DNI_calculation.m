%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to calculate direct irradiation
%
% @author Marco Pau <mpau@eonerc.rwth-aachen.de>
% @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% PVgenerator
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [DNI] = DNI_calculation(Extraterr_radiation, theta_zenith)

% h0 = pi/2 - theta_zenith;
% Dh0ref = 0.061359.*(0.1594 + 1.123.*h0 + 0.065656*(h0.^2))./(1 + 28.9344.*h0 + 277.3971*(h0.^2));
% h0ref = h0 + Dh0ref;
% Tlk = 4.5;    % value of Linke's turbidity factor when the sky is fully clean and dry (cit. Comparison Between Atmospheric Turbidity Coefficients of Desert and Temperate Climates)
Tlk = 3;
% m = 1/(cos(theta_zenith) + 0.15.*(93.885 - theta_zenith*180/pi).^(-1.253));    % relative optical air mass (cit. Comparison Between Atmospheric Turbidity Coefficients of Desert and Temperate Climates)
% m = 1./(sin(h0ref) + 0.50572*(h0ref + 6.07995).^(-1.6364));
% Air mass calculation: reference of formula -> formula of Young (1994) on: https://en.wikipedia.org/wiki/Air_mass_%28astronomy%29#CITEREFYoung1994
m = (1.002432.*(cos(theta_zenith).^2) + 0.148386.*cos(theta_zenith) + 0.0096467) ./ ((cos(theta_zenith).^3) + 0.149864.*(cos(theta_zenith).^2) + 0.0102963.*cos(theta_zenith) + 0.000303978);

% Computation spectral optical thickness of the clean dry atmosphere dr
% (http://re.jrc.ec.europa.eu/pvgis/solres/solmod3.htm Kasten 1996)
if m <=20
    dr = 1./(6.6296 + 1.7513.*m - 0.1202.*(m.^2) + 0.0065*(m.^3) - 0.00013.*(m.^4));
else
    dr = 1./(10.4 + 0.718.*m);
end

DNI = Extraterr_radiation.*exp(-0.8662*Tlk.*m.*dr);




