%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to calculate reduction factor
%
% @author Marco Pau <mpau@eonerc.rwth-aachen.de>
% @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% PVgenerator
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [d_dir, d_dif] = reduction_factor_creation_new(start_time, end_time, cloudiness)

len = end_time - start_time;

if cloudiness == 1
    d_dir = zeros(1,len);
    d_dif = zeros(1,len);
else
    load('Transition_matrix.mat');
    load('White_covariance_matrix.mat');

    F_matrix = reshape(F_matrix_out_tot(cloudiness,:,:),[2 2]);
    cov_w1 = reshape(cov_w1_out_tot(cloudiness,:,:),[2 2]);
    cov_w1 = (cov_w1 + cov_w1.')/2;
    d(1:2,1) = mvnrnd([0;0],cov_w1);
% F_matrix
% cov_w1

    for x = 2 : len
        d(1:2,x) = F_matrix * d(1:2,x-1) + mvnrnd([0;0],cov_w1)';   
    end
    d_dir = d(1,:);
    d_dif = d(2,:);
end
